import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productsbar',
  templateUrl: './productsbar.component.html',
  styleUrls: ['./productsbar.component.sass']
})
export class ProductsbarComponent implements OnInit {

  showFilters:boolean=true;

  showHideFilters(){
    this.showFilters=!this.showFilters;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
