import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsbarComponent } from './productsbar.component';

describe('ProductsbarComponent', () => {
  let component: ProductsbarComponent;
  let fixture: ComponentFixture<ProductsbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductsbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
