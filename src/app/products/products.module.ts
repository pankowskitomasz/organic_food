import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products/products.component';
import { ProductsbarComponent } from './productsbar/productsbar.component';
import { ProductslistComponent } from './productslist/productslist.component';


@NgModule({
  declarations: [
    ProductsComponent,
    ProductsbarComponent,
    ProductslistComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule
  ]
})
export class ProductsModule { }
